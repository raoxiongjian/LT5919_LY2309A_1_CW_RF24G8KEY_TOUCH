#ifndef SPI_H_
#define SPI_H_

extern unsigned char RegH;
extern unsigned char RegL;

extern unsigned char RF_Time;			//用于控制RF发送间隔时间
extern unsigned char idata	DD_T,YK_DM,D0,D1,key_fz,key_jz,key_t2,mode,send,suiji;
extern unsigned int sehuan;

void SPI_WriteReg(unsigned char, unsigned char, unsigned char);
void SPI_ReadReg(unsigned char);
void SPI_WriteReg8(unsigned char H);
void LT8900_Init(void);
void RF_Send(unsigned char *u8Buff, unsigned char u8Len);//void RF_Send();

unsigned long int GetChipID(void);

sbit RST        = P3^4;
sbit SPI_SS     = P3^1;			    
sbit SPI_CLK    = P3^0;             // SCL 
sbit MOSI       = P3^5;
sbit MISO       = P1^6;             // SDA 
//sbit PKT        = P1^5;
sbit LED        = P1^6;
#endif