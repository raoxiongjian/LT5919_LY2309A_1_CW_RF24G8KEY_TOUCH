#ifndef _SPI_C_
#define _SPI_C_

#include "ca51f_config.h"
#include "includes\ca51f3sfr.h"
#include "includes\ca51f3xsfr.h"
#include "includes\gpiodef_f3.h"
#include "Library\includes\delay.h"
#include "spi.h"


//       读写FIFO 16位缓存器          //
unsigned char RegH;
unsigned char RegL;              //Used to store the Registor Value which is read.
unsigned char RF_Time;			//用于控制RF发送间隔时间
unsigned char idata	DD_T,YK_DM,D0,D1,key_fz,key_jz,key_t2,mode;	
unsigned int sehuan = 0;
//一包依次发送的数据 包数据字节数、遥控器识别编号、地址低字节、地址高字节、圆环数据、按键值、包序号
unsigned char idata	JM_DD_T,JM_YK_DM,JM_D0,JM_D1,JM_sehuan,JM_key_fz,JM_key_jz,key_t2,mode,suiji,JM_suiji;

unsigned char idata	channel,send,tx_tt;
unsigned char idata	channel_TAB[3]={0x08,0x4a,0x74};		//发射信道

/*****************************************/
// SPI Write and Read ,as well as Read8  // 
/*****************************************/
unsigned long int GetChipID(void)	//滚码ID获取			
{
	unsigned long int u32ID;
	LOCK  = 0x2B;	
	FSCMD = 0x80;	
	PTSH = 0x01;				
	PTSL = 0x00;        			
	FSCMD = 0x81;						
	u32ID = FSDAT;
	u32ID *= 256;
	u32ID |= FSDAT;
	u32ID *= 256;
	u32ID |= FSDAT;
	u32ID *= 256;
	u32ID |= FSDAT;
	LOCK = 0xAA;
	FSCMD = 0;
	return u32ID;
}
//---------------------------------------------------------------------------
void SPI_WriteReg(unsigned char addr, unsigned char H, unsigned char L)
{
	unsigned char i;

	SPI_SS = 0;

	for(i = 0; i < 8; ++ i)
	{
		MOSI = addr & 0x80;

		SPI_CLK = 1;                       //capturing at the down side.
		addr = addr << 1;                    //There is no Delay here. determines the rate of SPI.
		SPI_CLK = 0;
	}
	for(i = 0; i < 8; ++i)                 //Write H
	{
		
		MOSI = H & 0x80;
		SPI_CLK = 1;
		H = H << 1;
		SPI_CLK = 0;
	}
	for(i = 0; i < 8; ++i)                 //Write L
	{  
		MOSI = L & 0x80;

		SPI_CLK = 1;
		L = L << 1;
		SPI_CLK = 0;	
	}
//	_nop_();	  //nop
	SPI_SS = 1;
}

void SPI_WriteReg8(unsigned char H)
{
	unsigned char i;

	for(i = 0; i < 8; ++i)                 //Write H
	{
		
		MOSI = H & 0x80;
		SPI_CLK = 1;
		H = H << 1;
		SPI_CLK = 0;
	}

}


//void SPI_ReadReg(unsigned char addr)
//{
//	int i;
//	
//	SPI_SS = 0;
//	addr += 0x80;                    //when reading a Register,the Address should be added with 0x80.
//
//	for(i = 0; i < 8; ++ i)
//	{
//		MOSI = addr & 0x80;
//
//		SPI_CLK = 1;
//		addr = addr << 1;                      //Move one bit to the left.
//		SPI_CLK = 0;	
//	}
//
//	for(i = 0; i < 8; ++ i)
//	{
//		SPI_CLK = 1;                         //transmit at the up side.
////		_nop_();	  //nop
//		SPI_CLK = 0;
//
//		RegH = RegH << 1;  
//		RegH |= MISO;
//	}
//
//	
//	
//	for(i = 0; i < 8; ++ i)
//	{
//		SPI_CLK = 1;                         //transmit at the up side.
////		_nop_();	  //nop
//		SPI_CLK = 0;
//
//		RegL = RegL << 1; 
//		RegL |= MISO;
//	}
//
//	SPI_SS = 1;
//}

/*
void SPI_ReadReg8(unsigned char add)
{
	int i;
	
	SPI_SS = 0;
	add += 0x80;                                //when reading a Register,the Address should be added with 0x80.

	for(i = 0; i < 8; ++ i)
	{
		MOSI = add & 0x80;

		SPI_CLK = 1;
		SPI_CLK = 0;

		add = add << 1;                        //Move one bit to the left.
	}

	for(i = 0; i < 8; ++ i)
	{
		SPI_CLK = 1;                           //transmit at the up side.  MSB
		SPI_CLK = 0;

		RegH = RegH << 1;  
		RegH |= MISO;
	}

	SPI_SS = 1;
}
*/
//==============================================================
void RF_Send(unsigned char *u8Buff, unsigned char u8Len)
{
    unsigned char i = 0;
    
	SPI_SS = 0;
	Delay_ms(2);
	key_t2++;
	channel=0;

	for(tx_tt = 0; tx_tt <20; ++ tx_tt)
	{
		SPI_WriteReg( 7, 0x00, 0x00 );
		SPI_WriteReg( 52, 0x80, 0x80 );
		SPI_SS = 0;
		SPI_WriteReg8(50);
		SPI_WriteReg8(u8Len);
 
        for(i=0; i<u8Len; i++)
        {
            SPI_WriteReg8(u8Buff[i]);
        }
		SPI_SS = 1;
		SPI_WriteReg( 7, 0x01, channel_TAB[channel] );	  //send
		channel++;
		if(channel>2)channel=0;
		Delay_50us(16);			//控制间隔时间			
	}
	SPI_WriteReg( 7, 0x00, 0x00 );
	//SPI_WriteReg( 35, 0x43, 0x00 );	 //sleep
}

/***************************************/
// LT8900 Initializing Function        //
/***************************************/

void LT8900_Init(void)
{
	SPI_WriteReg( 0, 0x6f, 0xe0 );
	SPI_WriteReg( 1, 0x56, 0x81 );
	SPI_WriteReg( 2, 0x66, 0x17 );
	SPI_WriteReg( 4, 0x9c, 0xc9 );
	SPI_WriteReg( 5, 0x66, 0x37 );
	SPI_WriteReg( 7, 0x00, 0x4C );							  //channel is 2402Mhz
	SPI_WriteReg( 8, 0x6c, 0x90 );
	SPI_WriteReg( 9, 0x48, 0x00 );			  				  //PA -12.2dbm
	SPI_WriteReg(10, 0x7f, 0xfd );
	SPI_WriteReg(11, 0x00, 0x08 );
	SPI_WriteReg(12, 0x00, 0x00 );
	SPI_WriteReg(13, 0x48, 0xbd );
	SPI_WriteReg(22, 0x00, 0xff );
	SPI_WriteReg(23, 0x80, 0x05 );
	SPI_WriteReg(24, 0x00, 0x67 );
	SPI_WriteReg(25, 0x16, 0x59 );
	SPI_WriteReg(26, 0x19, 0xe0 );
	SPI_WriteReg(27, 0x13, 0x00 );
	SPI_WriteReg(28, 0x18, 0x00 );
	SPI_WriteReg(32, 0x48, 0x00 );
	SPI_WriteReg(33, 0x3f, 0xc7 );
	SPI_WriteReg(34, 0x20, 0x00 );
	SPI_WriteReg(35, 0x03, 0x00 );				    /* 重发次数为9次 一共发3次*/

	SPI_WriteReg(36, 0x9a, 0xab );
//	SPI_WriteReg(37, 0xbc, 0xcd );
//	SPI_WriteReg(38, 0x44, 0x02 );
	SPI_WriteReg(39, 0xbc, 0xcd );

	SPI_WriteReg(40, 0x44, 0x02 );
	SPI_WriteReg(41, 0xb0, 0x00 );	                /*CRC is ON; scramble is OFF; AUTO_ACK is OFF*/
	SPI_WriteReg(42, 0xfd, 0xb0 );					/*等待RX_ACK时间 176us*/
	SPI_WriteReg(43, 0x00, 0x0f );	
	/*baud rate 01=1M,04=250k,08=125K,10=62.5K*/				
	SPI_WriteReg(44, 0x04, 0x00 );		//1m
//	SPI_WriteReg(45, 0x05, 0x52 );		 
//	SPI_WriteReg(50, 0x00, 0x00 );

}

/*********************************************************************************************************************/
#endif

