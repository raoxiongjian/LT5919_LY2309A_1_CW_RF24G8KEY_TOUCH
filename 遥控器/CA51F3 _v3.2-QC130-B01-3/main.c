#ifndef _MAIN_C_
#define _MAIN_C_
/*********************************************************************************************************************/
#include "ca51f_config.h"
#include "includes\ca51f3sfr.h"
#include "includes\ca51f3xsfr.h"
#include "includes\gpiodef_f3.h"

#include "Library\includes\delay.h"
#include "Library\includes\uart.h"
#include "Library\includes\system_clock.h"
#include "Library\Includes\rtc.h"	
#include "Library\Includes\pwm.h"	
#include "includes\system.h"
#include "Library\includes\adc.h"
/*********************************************************************************************************************/
#include "TS_Lib\Includes\ts_configuration.h"
#include "TS_Lib\Includes\ts_def.h"
#include "TS_Lib\Includes\ts_api.h"
#include "TS_Lib\Includes\ts_service.h"
/*********************************************************************************************************************/	
#include "spi.h"
#include "intrins.h"
#define GROUP_ALL 0
#define GROUP1    1
#define GROUP2    2
#define GROUP3    3
#define GROUP4    4

#define TX_BUFF_LEN   13
#define IDENTIFICATION_CODE_INDEX      0
#define DATA_POINT_INDEX               1
#define KEY_CODE_INDEX                 2
#define PACK_NUM_INDEX                 3
#define ADDR1_INDEX                    4
#define ADDR2_INDEX                    5
#define ADDR3_INDEX                    6
#define ZID_INDEX                      7
#define GROUP_NUM_INDEX                8
#define RANDOM_INDEX                   9
#define VALUE_INDEX                    10
#define VALUE2_INDEX                   11
#define TX_NUM_INDEX                   13              //协议中的包内号

#define DPID_SWITCH                    1
#define DPID_ON                        2
#define DPID_OFF                       3
#define DPID_BRIGHTNESS_SET            4
#define DPID_BRIGHTNESS_INC            5
#define DPID_BRIGHTNESS_DEC            6
#define DPID_CW_SET                    7
#define DPID_CW_INC                    8
#define DPID_CW_DEC                    9
#define DPID_COLOUR_SET                10
#define DPID_COLOUR_INC                11
#define DPID_COLOUR_DEC                12
#define DPID_SATURATION_SET            13
#define DPID_SATURATION_INC            14
#define DPID_SATURATION_DEC            15
#define DPID_SPEED_SET                 16
#define DPID_SPEED_INC                 17
#define DPID_SPEED_DEC                 18
#define DPID_MODE_SET                  19
#define DPID_MODE_INC                  20
#define DPID_MODE_DEC                  21
#define DPID_SINGLE_WHITE              22
#define DPID_NIGHT_LIGHT               25

unsigned int  TS_Key_TM;
unsigned char temp,send_t1;
unsigned int temp_huan;
bit flag_sleep;
unsigned long int ID = 0;
    
static unsigned char RawDataBuff[TX_BUFF_LEN] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
static unsigned char EncryptedDataBuff[TX_BUFF_LEN] = {0};
/********************************************************************************************************
说明：
------------------------------------------------------------------------------------------------------------
1.ts_configuration.h是触摸的配置文件，可在配置文件中设置触摸通道、灵敏度等。详见ts_configuration.h中的说明。
------------------------------------------------------------------------------------------------------------
2.如果需要使用UART来调试，可在ca51f5_config.h中打开宏定义UART0_EN
------------------------------------------------------------------------------------------------------------
3.触摸库对外的数据接口为KeysFlagSN，用户可根据KeysFlagSN的值来判断触摸键是否按下（如果触摸键按下，KeysFlagSN
的对应位一直为1，触摸键松开，对应位变为0），TS_Key是根据KeysFlagSN的值生成的按键消息，用户如不使用可忽略。
------------------------------------------------------------------------------------------------------------
4.使能了触摸省电模式后，在无按键时，在定义的时间后会进入省电模式，进入省电模式前会关闭TMC中断和触摸中断（注意，总
中断仍是开启的），在触摸省电模式下，CPU会进入STOP模式，在被触摸唤醒后，程序在省电函数（TS_EnterSleepMode）的循环内运行（注意，触摸省电模式程序不
会在主循环内运行），在触摸省电模式下如果还要响应其他中断，用户需在进入触摸省电模式前自行开启其他中断，
如果需要退出省电模式，用户只需要在省电函数循环内加入条件判断，跳出循环即可。

变量TS_SleepEn是控制触摸省电模式的开关，如果希望程序在某些条件下不进入省电模式，只需在此条件下设置TS_SleepEn = 0；


------------------------------------------------------------------------------------------------------------
5.触摸外挂电容(即TK_CAP引脚连接电容)范围：6nF~50nF,建议值：20nF（即203）
------------------------------------------------------------------------------------------------------------
6.触摸引脚串联电阻范围：0~5K，建议值：1K欧姆，如果有抗对讲机等电磁干扰要求， 此电阻至少3K以上。
********************************************************************************************************/
unsigned char read_inner_trim(void)				
{
	unsigned char value;
	FSCMD = 0x80;	
	PTSH = 0x00;				
	PTSL = 0x24;      
	FSCMD = 0x81;						
	value = FSDAT;
	FSCMD = 0;
	return value;
}

unsigned char Cror(unsigned char u8Byte, unsigned char u8Cnt)
{
    unsigned char u8Temp = 0, i = 0;
    unsigned char u8Value = u8Byte;
    
    for(i=0; i<u8Cnt; i++)
    {
        u8Temp = ((u8Value & 0x01) << 7);
        u8Value = (u8Value >> 1) | u8Temp;
    }
    
    return u8Byte;
}

unsigned char GetCheckSum(unsigned char *u8pBuff, unsigned char u8Len)
{
    unsigned char u8CheckSum = 0, i = 0;
    
    for(i=0; i<u8Len; i++)
    {
        u8CheckSum += u8pBuff[i];
    }
    
    return u8CheckSum;
}

void DataEncryptionProcessing(unsigned char *RawData, unsigned char *EncryptedData, unsigned char u8Len)
{
    unsigned char i = 0;
    
    if((RawData == 0) || (EncryptedData == 0))
    {
        return;
    }
    
    if(u8Len == 0)
    {
        return;
    }
    
    for(i=0; i<u8Len; i++)
    {
        if(i != RANDOM_INDEX)
        {
            EncryptedData[i] = RawData[i] ^ RawData[RANDOM_INDEX];
        }
        else
        {
            EncryptedData[i] = RawData[i];
        }
    }
    
    EncryptedData[0] ^= 0x20;
    EncryptedData[1] ^= 0x22;
    EncryptedData[2] ^= 0x05;
    EncryptedData[3] ^= 0x21;
    
    EncryptedData[RANDOM_INDEX] = _cror_(EncryptedData[RANDOM_INDEX], 2);
}

void SystemInit(void)
{
#ifdef LVD_RST_ENABLE
	LVDCON = 0x80;	
	Delay_50us(1);
	LVDCON = 0xc2;	
	LVDCON |= 0x20;	
#endif
#if (SYSCLK_SRC == PLL)
	Sys_Clk_Set_PLL(PLL_Multiple);	
#endif
#ifdef UART0_EN
	Uart0_Initial(UART0_BAUTRATE);
#endif
#ifdef UART1_EN
	Uart1_Initial(UART1_BAUTRATE);
#endif
#ifdef UART2_EN
	Uart2_Initial(UART2_BAUTRATE);
#endif
	
	ADCFGH = (ADCFGH&0xC0) | VTRIM(read_inner_trim());	//加载ADC内部基准校准值
}
void main(void)
{
	SystemInit();
	P30F=OUTPUT;		//sclk	  	out
	P31F=OUTPUT;		//ss	  	out
	P34F=OUTPUT;		//rst		out
	P35F=OUTPUT;		//si		out
	P16F=OUTPUT;	 	//so  led

	LED=0;
	RST  = 0;	
	Delay_ms(50);
	RST  = 1;
	Delay_ms(100);
	LT8900_Init();
	Delay_ms(10);
	RF_Send(EncryptedDataBuff, TX_BUFF_LEN);
	Delay_ms(10);
	SPI_WriteReg( 35, 0x43, 0x00 );	 //2.4g sleep

	EA = 1;
	TS_init();
		
    LED=1;
	DD_T=8;
	YK_DM=0xB0;
	ID = (unsigned int)GetChipID();	  //获取遥控ID
    
    RawDataBuff[IDENTIFICATION_CODE_INDEX] = 0x40;
    RawDataBuff[ADDR1_INDEX] = ID >> 16;
    RawDataBuff[ADDR2_INDEX] = ID >> 8;
    RawDataBuff[ADDR3_INDEX] = ID;
    RawDataBuff[GROUP_NUM_INDEX] = GROUP_ALL;   //开机默认分组号为0xff
    
	while(1)
	{			
		TS_Action();
		suiji++;	//加密随机数
#if SUPPORT_WHEEL_SLIDER
		if(WheelSliderPosition[0] != -1)
		{
			//当WheelSliderPosition不等于-1时， 表示滑条或圆环有触摸事件发生。WheelSliderPosition的值表示滑条或圆环的位置。
			LED=0;
			RF_Time=0;
			sehuan=WheelSliderPosition[0];
            
			if(sehuan!=temp_huan)
			{
                temp_huan=sehuan;
                
                if(sehuan < 20)
                {
                    sehuan = 20;
                }
                else if(sehuan > 235)
                {
                    sehuan = 235;
                }
                
				send=1;
                RawDataBuff[DATA_POINT_INDEX] = DPID_CW_SET;
                RawDataBuff[VALUE_INDEX] = ((unsigned int)sehuan - 20) * 100 / 215;
			}
		}
//		if(WheelSliderPosition[1] != -1)
//		{
//			//当WheelSliderPosition不等于-1时， 表示滑条或圆环有触摸事件发生。WheelSliderPosition的值表示滑条或圆环的位置。
//			LED=0;
//			RF_Time=0;
//			sehuan=WheelSliderPosition[1];
//			if(sehuan!=temp_huan)
//			{
//				send=1;
//				temp_huan=sehuan;
//                RawDataBuff[DATA_POINT_INDEX] = DPID_BRIGHTNESS_SET; 
//                if(sehuan < 30)
//                {
//                    sehuan = 30;
//                }
//                else if(sehuan > 180)
//                {
//                    sehuan = 180;
//                }
//                sehuan =(sehuan - 30) * 100 / 150;

//                RawDataBuff[VALUE_INDEX] = sehuan;
//                if(RawDataBuff[VALUE_INDEX] == 0)
//                {
//                    RawDataBuff[VALUE_INDEX] = 1;
//                }
//			}
//		}
#endif

#if GENERATE_TS_KEY_EN
/*************************************************************************************************
变量TS_Key是根据KeysFlagSN得到的按键信息，以下为按键产生的过程：

*************************************************************************************************/
		if(TS_Key != 0)
		{
            TS_Key_TM=TS_Key&0xF000;
            
            if(TS_Key_TM == 0)
            {
                switch(TS_Key & 0x00ff)
                {
                    //单按键消息
                    case K_on:									//K1短按产生的按键消息	
                        LED=0;
                        RawDataBuff[DATA_POINT_INDEX] = DPID_ON;
                        RawDataBuff[GROUP_NUM_INDEX] = GROUP1;
                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
                        send=1;
                        break;
                    //单按键消息
                    case K_off:									//K1短按产生的按键消息	
                        LED=0;
                        RawDataBuff[DATA_POINT_INDEX] = DPID_OFF;
                        RawDataBuff[GROUP_NUM_INDEX] = GROUP1;
                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
                        send=1;
                        break;
                    case K_bn_up:									//K1短按产生的按键消息	
                        LED=0;
                        RawDataBuff[DATA_POINT_INDEX] = DPID_BRIGHTNESS_INC;
                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
                        send=1;
                        break;
                    case K_bn_down:									//K1短按产生的按键消息	
                        LED=0;
                        RawDataBuff[DATA_POINT_INDEX] = DPID_BRIGHTNESS_DEC;
                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
                        send=1;
                        break;
//                    case K_sp_up:									//K1短按产生的按键消息	
//                        RawDataBuff[DATA_POINT_INDEX] = DPID_SPEED_INC;
//                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
//                        send=1;
//                        break;
//                    case K_sp_down:									//K1短按产生的按键消息	
//                        RawDataBuff[DATA_POINT_INDEX] = DPID_SPEED_DEC;
//                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
//                        send=1;
//                        break;
//                    case K_mode_up:									//K1短按产生的按键消息	
//                        RawDataBuff[DATA_POINT_INDEX] = DPID_MODE_INC;
//                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
//                        send=1;
//                        break;
//                    case K_mode_down:									//K1短按产生的按键消息	
//                        RawDataBuff[DATA_POINT_INDEX] = DPID_MODE_DEC;
//                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
//                        send=1;
//                        break;
                }
            }
            else if((TS_Key_TM == KEY_LONG)||(TS_Key_TM == KEY_LONG_START))
			{
                switch(TS_Key & 0x00ff)
                {
                    //单按键消息
                    case K_on:									//K1短按产生的按键消息	
                        LED=0;
                        RawDataBuff[DATA_POINT_INDEX] = DPID_SINGLE_WHITE | 0x80;
                        RawDataBuff[GROUP_NUM_INDEX] = GROUP1;
                        send=1;
                        break;
                    //单按键消息
                    case K_off:									//K1短按产生的按键消息	
                        LED=0;
                        RawDataBuff[DATA_POINT_INDEX] = DPID_NIGHT_LIGHT | 0x80;
                        RawDataBuff[GROUP_NUM_INDEX] = GROUP1;
                        send=1;
                        break;
                    case K_bn_up:									//K1短按产生的按键消息
                        LED=0;                        
                        RawDataBuff[DATA_POINT_INDEX] = DPID_BRIGHTNESS_INC  | 0x80;
                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
                        send=1;
                        break;
                    case K_bn_down:									//K1短按产生的按键消息
                        LED=0;                        
                        RawDataBuff[DATA_POINT_INDEX] = DPID_BRIGHTNESS_DEC  | 0x80;
                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
                        send=1;
                        break;
//                    case K_sp_up:									//K1短按产生的按键消息	
//                        RawDataBuff[DATA_POINT_INDEX] = DPID_SPEED_INC  | 0x80;
//                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
//                        send=1;
//                        break;
//                    case K_sp_down:									//K1短按产生的按键消息	
//                        RawDataBuff[DATA_POINT_INDEX] = DPID_SPEED_DEC  | 0x80;
//                        RawDataBuff[KEY_CODE_INDEX] = TS_Key & 0x00ff;
//                        send=1;
//                        break;
                }
			}
            else if(TS_Key_TM == KEY_BREAK)
            {
                send=2;
            }
		}
//-----------------------------------------------------------------
		if(KeysFlagSN != 0)
		{
			RF_Time=0;
		}
		if(RF_Time>10)
		{
			LED=1;
			send_t1=0;
		}
		if(RF_Time>50&&send==2)
		{
			LT8900_Init();
			RF_Time=0;
			send=0;
//			send_t1=0;
//			flag_sleep=1;
			SPI_WriteReg( 35, 0x43, 0x00 );	 //2.4g sleep	
//			LED=~LED;	
		}
//-----------------------------------------
		if(send==1)
		{
//			if(flag_sleep)	//表示从休眠唤醒开始发射，需要先唤醒2.4G芯片
//			{
//				SPI_SS = 0;
//				Delay_ms(2);
//				key_t2++;
//				flag_sleep=0;
//				LED=1;	
//			}
//			else 
//			if(send_t1==0)	//表示按键数据有改变重新发一帧
//			{
//				key_t2++;	
//			}
            
            static unsigned char s_u8PackgeNum = 0;
            
            s_u8PackgeNum++;
            RawDataBuff[PACK_NUM_INDEX] = s_u8PackgeNum;
            
            DataEncryptionProcessing(RawDataBuff, EncryptedDataBuff, TX_BUFF_LEN - 1);
            
			EA = 0;
			RF_Send(EncryptedDataBuff, TX_BUFF_LEN);
			EA = 1;
//			send_t1++;
//			if(send_t1>5)
//			{
//				send=2;		
//			}
			send=2;	
//			LED=~LED;
		}
		
//#endif
#endif
	}
}
#endif
