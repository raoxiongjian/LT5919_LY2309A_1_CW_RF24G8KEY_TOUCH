/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_dm.h
* 文件标识：
* 摘 要：
*    存储模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月21日
*/
#include "glh_dm.h"
#include "glh_flash.h"
#include "glh_sys_tick.h"

#ifdef SUPPORT_DM

#define DM_BUFF_LEN    GLHDM_SIZE
#define DM_WRITE_INTERVAL       500        // 500ms
#define DM_SAVE_FLASH_ADDR      (GLHFLASH_END_ADDR-GLHFLASH_PAGE_SIZE+1)  // FLASH的最后一页作为DM的存储区域

static uint8 s_u8DmBuff[DM_BUFF_LEN] = {0};
static BOOL s_bWriteFlash = FALSE;  //是否需要写入到flash
static uint32 s_u32DmTick = 0; //

void GLHDM_Init(void)
{
    GLHFLASH_Init();

    GLHFLASH_ReadBytes(DM_SAVE_FLASH_ADDR, s_u8DmBuff, DM_BUFF_LEN);
    s_bWriteFlash = FALSE;
    s_u32DmTick = GulSystickCount + DM_WRITE_INTERVAL;
}

void GLHDM_ReadBytes(uint16 u16Addr, uint8 *pu8Buff, uint16 u16Len)
{
    memcpy(pu8Buff, s_u8DmBuff+u16Addr, u16Len);
}

void GLHDM_WriteBytes(uint16 u16Addr, const uint8 *pu8Buff, uint16 u16Len)
{  
    memcpy(s_u8DmBuff+u16Addr, pu8Buff, u16Len);
    
    s_bWriteFlash = TRUE;
}

void GLHDM_MainThread(void)
{
    if(GulSystickCount > s_u32DmTick)
    {
        if (s_bWriteFlash)
        {
            s_bWriteFlash = FALSE;
            GLHFLASH_WriteBytes(DM_SAVE_FLASH_ADDR, s_u8DmBuff, DM_BUFF_LEN);
        }
        s_u32DmTick = GulSystickCount + DM_WRITE_INTERVAL;
    }
}

#endif
