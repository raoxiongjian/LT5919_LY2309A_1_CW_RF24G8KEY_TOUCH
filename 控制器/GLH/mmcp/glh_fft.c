#include "glh_fft.h"

data struct compx temp;  
 
code float iw[64]=
{
	1.000,0,0.9952,-0.0980,0.9808,-0.1951,0.9569,-0.2903,0.9239,-0.3827,0.8819,-0.4714,0.8315,-0.5556,
	0.7730,-0.6344,0.7071,-0.7071,0.6344,-0.7730,0.5556,-0.8315,0.4714,-0.8819,0.3827,-0.9239,0.2903,-0.9569,
	0.1951,-0.9808,0.0980,-0.9952,0.0,-1.0000,-0.0980,-0.9952,-0.1951,-0.9808,-0.2903,0.9569,-0.3827,-0.9239,
	-0.4714,-0.8819,-0.5556,-0.8315,-0.6344,-0.7730,-0.7071,-0.7071,-0.7730,-0.6344,-0.8315,-0.5556,-0.8819,-0.4714,
	-0.9239,-0.3827,-0.9569,-0.2903,-0.9808,-0.1951,-0.9952,-0.0980
};


//复数乘法
void ee(struct compx b1,uchar data b2)
{ 
	temp.real=b1.real*iw[2*b2]-b1.imag*iw[2*b2+1];
	temp.imag=b1.real*iw[2*b2+1]+b1.imag*iw[2*b2]; 
} 
//乘方函数  
uint mypow(uchar data nbottom,uchar data ntop)
{
    uint data result=1;
    uchar data t;    
    for(t=0;t<ntop;t++)result*=nbottom; 
    return result;
}
//快速傅立叶变换  
void fft(struct compx *xin,uchar data N)
{
	uchar data  fftnum,i,j,k,l,m,n,disbuff,dispos,dissec;
	data struct compx t;
	fftnum=N;                         //傅立叶变换点数
	for(m=1;(fftnum=fftnum/2)!=1;m++);//求得M的值 
	for(k=0;k<=N-1;k++)               //码位倒置
	{
		n=k;
		j=0; 
		for(i=m;i>0;i--)             //倒置
		{
			j=j+((n%2)<<(i-1));
			n=n/2;
		} 
		if(k<j){t=xin[1+j];xin[1+j]=xin[1+k];xin[1+k]=t;}//交换数据
	}  
	for(l=1;l<=m;l++)                //FFT运算
	{
		disbuff=mypow(2,l);          //求得碟间距离
		dispos=disbuff/2;            //求得碟形两点之间的距离
		for(j=1;j<=dispos;j++)
			for(i=j;i<N;i=i+disbuff) //遍历M级所有的碟形
			{
				dissec=i+dispos;     //求得第二点的位置
				ee(xin[dissec],(uint)(j-1)*(uint)N/disbuff);//复数乘法
				t=temp;
				xin[dissec].real=xin[i].real-t.real;
				xin[dissec].imag=xin[i].imag-t.imag;
				xin[i].real=xin[i].real+t.real;
				xin[i].imag=xin[i].imag+t.imag;
			}
	}
} 