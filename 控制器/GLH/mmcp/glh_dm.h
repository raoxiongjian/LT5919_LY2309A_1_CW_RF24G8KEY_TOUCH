/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：jt_dm.h
* 文件标识：
* 摘 要：
*    存储模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月21日
*/
#ifndef __GLH_DM_H__
#define __GLH_DM_H__
#include "glh_typedef.h"
#include "glh_flash.h"

#ifndef GLHDM_SIZE
#define GLHDM_SIZE       16      // dm空间大小，应用层定义
#endif

#if (GLHDM_SIZE > GLHFLASH_PAGE_SIZE)
#error "JTDM_SIZE TOO LARGE!"
#endif

#if (GLHDM_SIZE <= 0)
#error "JTDM_SIZE ERROR!"
#endif

#define GLHDM_START_ADDR   0x0000     //起始地址
#define GLHDM_END_ADDR       (GLHDM_START_ADDR + GLHDM_SIZE - 1)   //结束地

/*
    功能: 初始化数据存储模块
    输入:
    输出: 
*/
void GLHDM_Init(void);

/*
    功能: 从dm中的指定位置读取指定长度的数据
    输入:
    	u16Addr 	        起始地址
    	pu8Buff 	        读取数据的保存缓存
    	u16Len		读取长度
    返回:
*/
void GLHDM_ReadBytes(uint16 u16Addr, uint8 *pu8Buff, uint16 u16Len);

/*
    功能: 从dm中的指定位置写入指定长度的数据
    输入:
    	u16Addr 	       起始地址
    	pu8Buff 	       写入数据的保存缓存
    	u16Len	       写入长度
    返回:
*/
void GLHDM_WriteBytes(uint16 u16Addr, const uint8 *pu8Buff, uint16 u16Len);

/*
    功能: DM主线程，需要在主线程中不停调用
    输入:
    返回:  
*/
void GLHDM_MainThread(void);

#endif //__GLH_DM_H__

