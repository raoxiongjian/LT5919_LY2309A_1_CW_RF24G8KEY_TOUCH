/*
* Copyright (c) 2021, 深圳市君光丽海技有限公司
* All rights reserved.
*
* 文件名称：glh_rf_433.h
* 文件标识：
* 摘 要： 
*  433M数据接收
* 当前版本：V1.0
* 作 者： 饶雄健
* 完成日期：2021年6月25日
*/
#ifndef __GLH_RF_433_H__
#define __GLH_RF_433_H__
#include "glh_typedef.h"
#include "gpio.h"


#ifndef JTRF433_PORT
#define JTRF433_PORT   GpioPort2
#endif

#ifndef JTRF433_PIN
#define JTRF433_PIN    GpioPin6
#endif



//全局数据
extern uint32 g_u32RfData;  //数据

/*
    功能: 数据接收完成后的回调函数
    接收到的数据由g_u32RfData提供
*/
extern void GLHRF433_EventCb(void);

/*
    功能: 初始化433模块
    输入: pfnCallback 数据接收完成回调
    返回值: 无
*/
void GLHRF433_Init(void);

/*
    功能: 模块驱动主线程，需要实时调用
    输入: 无
    返回值: 无
*/
void GLHRF433_MainThread(void);

#endif //__JT_RF_433_H__
