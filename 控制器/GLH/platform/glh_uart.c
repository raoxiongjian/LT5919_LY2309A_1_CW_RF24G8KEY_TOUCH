/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_uart.h
* 文件标识：
* 摘 要：
*   串口通信程序，模拟实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021.7.21
*/
#include "glh_uart.h"
#include "include/ca51f003_config.h"		
#include "include/ca51f003sfr.h"
#include "include/ca51f003xsfr.h"
#include "include/gpiodef_f003.h"
#include "glh_pwm.h"

#define RB81_FLAG (1 << 2)
#define UART_DIV   8   //8分频    最小为6   最大为32
#define UART_CNT   8
#define DEVICE_ID_MAX 512

#ifndef UART_REC_BUFF_LEN
#define GLHUART_REC_BUFF_LEN      64     //串口缓冲区大小
#endif

static uint8 XDATA s_u8Wp = 0;  
static uint8 XDATA s_u8Rp = 0;
uint8 XDATA s_au8RecBuff[GLHUART_REC_BUFF_LEN] = {0};
static uint8 s_u8UartNum = 0; 
static uint8 s_u8R = 0,s_u8G = 0,s_u8B = 0,s_u8W = 0;

static void iWriteByte(uint8 byte)
{
    s_au8RecBuff[s_u8Wp] = byte;
    s_u8Wp++;
	if(s_u8Wp == GLHUART_REC_BUFF_LEN)
	{
		s_u8Wp = 0;
	}
}

void GLHUART_Init(void)
{
	UDCKS1 = 0x80 | (UART_DIV - 1);
	
   	GPIO_Init(P07F,P07_UART1_RX_SETTING);
	GPIO_Init(P06F,P06_UART1_TX_SETTING); 

	S1RELH = (unsigned char)((0x400 - UART_CNT) >> 8);
	S1RELL = (unsigned char)(0x400 - UART_CNT);

	S1CON = 0x50;   //S1CON = 0x50;//(9位数据)       //S1CON = 0xD0;//(8位数据)
	ES1 = 1;
}

void GLHUART_SendByte(uint8 u8Byte)
{
	S1BUF = u8Byte;
	
	while((S1CON & 0x02) == 0);
	S1CON = (S1CON&~(0x03))|0x02;	
}

void GLHUART_SendBuff(const uint8 *pu8Buff, uint8 u8Len)
{
    uint8 i = 0;
    if (pu8Buff != NULL && u8Len > 0)
    {
        for (i=0; i<u8Len; i++)
        {
            GLHUART_SendByte(pu8Buff[i]);
        }
    }
}

void GLHUART_MainLoop(void)
{
//    while (s_u8Rp != s_u8Wp)
//    {
//        GLHUART_RecCallback(s_au8RecBuff[s_u8Rp]);
//		
//        s_u8Rp++;
//		if(s_u8Rp == GLHUART_REC_BUFF_LEN )
//		{
//		    s_u8Rp = 0;
//		}
//    }
	
//	GLHPWM_Set(PWM_CHANNEL_R , s_u8R);
//	GLHPWM_Set(PWM_CHANNEL_G , s_u8G);
//	GLHPWM_Set(PWM_CHANNEL_B , s_u8B);
//	GLHPWM_Set(PWM_CHANNEL_W , s_u8W);
}

void UART1_ISR (void) interrupt 6	
{
	static uint16 s_u16UartRecNum = 0;
	static BOOL s_bDataValidFlag = FALSE;

	if(S1CON & 0x01)
	{
		S1CON = (S1CON&~(0x03))|0x01;	//清接收中断标记

		if((S1CON & RB81_FLAG) == 0)  
		{
			if(S1BUF == 0)  
			{
				s_u16UartRecNum = 0;
				s_bDataValidFlag = TRUE;
			}
			else
			{
				s_bDataValidFlag = FALSE;
			}
		}
		else
		{
			if(s_bDataValidFlag)
			{
			    GLHUART_RecCallback(s_u16UartRecNum, S1BUF);
				
				s_u16UartRecNum++;
				if(s_u16UartRecNum > DEVICE_ID_MAX)
				{
					s_bDataValidFlag = FALSE;
				}
			}
		}
	}
	
//	if(S1CON & 0x02)
//	{
//		S1CON = (S1CON&~(0x03))|0x02;	
//	}
}
