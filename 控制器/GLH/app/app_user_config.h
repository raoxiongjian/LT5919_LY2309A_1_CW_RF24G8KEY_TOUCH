/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称: app_user_config.h
* 文件标识：
* 摘 要：
*   应用层通用配置文件
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年6月21日
*/
#ifdef  __cplusplus
extern "C" {
#endif


//#define SUPPORT_WD          //是否支持看门狗
//#define SUPPORT_UART        //是否支持串口
#define SUPPORT_FLASH       //是否支持FLASH
//#define SUPPORT_RF433
//#define SUPPORT_DM          //支持DM模块
#define SUPPORT_KEY

#define SUPPORT_PWM
#define PWM_CHANNEL_R     E_PWM_CH2
#define PWM_CHANNEL_G     E_PWM_CH1
#define PWM_CHANNEL_B     E_PWM_CH3
#define PWM_CHANNEL_C     E_PWM_CH0
#define PWM_CHANNEL_W     E_PWM_CH4
#define COLOUR_MAX        255

#define H_MAX                    360    
#define S_MAX                    100.0
#define V_MAX                    100.0
	
#define OVER_CURRENT_DETECT_ADC_CH    JTADC_CHANNEL_8
	
#define XDATA   xdata
#define DATA    data
#define CODE    code
    
#define MIC_ADC_CHANNEL   GLHADC_CHANNEL_0

#define SUPPORT_MIC

#define GLHLM_MIC_ADC_BASE_VAL   (0)//(666)  //基准电压值 对应的ADC值0v
#define GLHLM_MIC_ADC_VALID_DELTA_VAL  (250)//(372)  //有效的adc差值,排除掉0.3V的干扰
#define GLHLM_MIC_ADC_MAX_DELTA_VAL  (1489) //最大的adc差值1.42
	
#ifdef  __cplusplus
}
#endif

