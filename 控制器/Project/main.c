#ifndef _MAIN_C_
#define _MAIN_C_

#include "glh_app_process.h"
#include "MS51_16K.H"

void main(void)
{
    GLHAPS_Init();  
    
	while(1)
	{
		GLHAPS_MainThread();
	}
}
#endif